﻿using System;
using System.Collections.Generic;
using Week5ListViewsAndBinding.Models;
using Xamarin.Forms;

namespace Week5ListViewsAndBinding
{
    public partial class MoreStudentInfo : ContentPage
    {
        public MoreStudentInfo()
        {
            InitializeComponent();
        }

        public MoreStudentInfo(Student student)
        {
            InitializeComponent();

            BindingContext = student;
        }
    }
}
