﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Week5ListViewsAndBinding.Models;
using Xamarin.Forms;

namespace Week5ListViewsAndBinding
{
    public partial class CustomListViewCells : ContentPage
    {
        public CustomListViewCells()
        {
            InitializeComponent();

            PopulateListView();

        }

        void HandleDeleteClicked(object sender, System.EventArgs e)
        {
        }

        void HandleMoreClicked(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var student = (Student)menuItem.CommandParameter;
            Navigation.PushAsync(new MoreStudentInfo(student));
        }

        private void PopulateListView()
        {
            var collection = new ObservableCollection<Student>();

            var student1 = new Student();
            student1.Name = "Bill Griffith";
            student1.GradeLevel = "Freshman";
            student1.Gpa = 3.9;
            student1.FavoriteHobby = "CS 481 Class";
            collection.Add(student1);

            var student2 = new Student
            {
                Name = "Rich",
                GradeLevel = "Senior",
                Gpa = 3.6,
                FavoriteHobby = "Tennis",
            };
            collection.Add(student2);

            ListViewWithCustomCells.ItemsSource = collection;
        }
    }
}
