﻿using System;
namespace Week5ListViewsAndBinding.Models
{
    public class Student
    {
        public string Name
        {
            get;
            set;
        }

        public string GradeLevel
        {
            get;
            set;
        }

        public double Gpa
        {
            get;
            set;
        }

        public string FavoriteHobby
        {
            get;
            set;
        }
    }
}
