﻿using Xamarin.Forms;

namespace Week5ListViewsAndBinding
{
    public partial class Week5ListViewsAndBindingPage : ContentPage
    {
        public Week5ListViewsAndBindingPage()
        {
            InitializeComponent();
        }

        void NavigateToCustomCells(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new CustomListViewCells());
        }
    }
}
